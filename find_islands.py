from collections import deque
from yaml import load
from os import path
from sys import argv

class Islands(object):
    def __init__(self, filename="islands.txt"):
        self.filename = filename

    def convert_txt_to_map_grid(self):
        dir = path.dirname(__file__)
        file_path = path.join(dir, self.filename)

        with open(file_path, "r") as f:
            data = load(f)
            if data is None:
                map_grid = [[]]
            elif isinstance(data, int):
                map_grid = [list(map(str, str(data)))]
            else:
                if len(data.split("\n")) > 1:
                    raise Exception("Sorry, only one matrix(map_grid) can be inside the islands.txt")
                map_grid = [list(x) for x in data.split(" ")]
        return map_grid

    def find_islands(self, map_grid):
        if not map_grid or not map_grid[0]:
            return 0

        if not isinstance(map_grid[0], list):
            return 0 if map_grid[0] == 0 else 1

        # The idea is to implement BFS algorithm
        islands = 0
        # Moves - up, down, left, right, top right, top left, down left, down right
        moves = [(0, 1), (0, -1), (-1, 0), (1, 0), (1, 1), (1, -1), (-1, -1), (-1, 1)]
        x, y = len(map_grid), len(map_grid[0])

        for xx in range(x):
            for yy in range(y):
                if map_grid[xx][yy] == '1':
                    islands += 1
                    q = deque()
                    q.append((xx, yy))
                    map_grid[xx][yy] = '0'
                    while q:
                        r, c = q.popleft()
                        for d in moves:
                            nr, nc = r + d[0], c + d[1]
                            # Corner conditions
                            if nr >= 0 and nr < x and nc >= 0 and nc < y and map_grid[nr][nc] == '1':
                                q.append((nr, nc))
                                map_grid[nr][nc] = '0'
        return islands

if __name__ == '__main__':
    args = argv[1::]
    if len(args) > 1:
        raise Exception("Too many arguments/files used as an input for find_islands.py")

    if len(args) == 0:
        raise Exception("Path to input file not given. Please add path to input file.")

    inst = Islands(args[0])
    map_grid = inst.convert_txt_to_map_grid()
    print(inst.find_islands(map_grid))
