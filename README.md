# Find islands on water


#### How to use the python script or find the islands on water.

* As an argument/input add just one file. The result will be printed to STDOUT.
wlodarczyk@ubuntu:~/PycharmProjects/islands_on_water$ ./find_islands.sh /home/wlodarczyk/PycharmProjects/islands_on_water/islands.txt 
4

* Script check if input file exists.
wlodarczyk@ubuntu:~/PycharmProjects/islands_on_water$ ./find_islands.sh /home/wlodarczyk/PycharmProjects/islands_on_water/islands2.txt 
Traceback (most recent call last):
  File "find_islands.py", line 62, in <module>
    map_grid = inst.convert_txt_to_map_grid()
  File "find_islands.py", line 14, in convert_txt_to_map_grid
    with open(file_path, "r") as f:
IOError: [Errno 2] No such file or directory: '/home/wlodarczyk/PycharmProjects/islands_on_water/islands2.txt'

* Only one file can be passed as an argument. If you add more then the following exception will be raised.
wlodarczyk@ubuntu:~/PycharmProjects/islands_on_water$ ./find_islands.sh /home/wlodarczyk/PycharmProjects/islands_on_water/islands.txt /home/wlodarczyk/PycharmProjects/islands_on_water/islands.tx2t
Traceback (most recent call last):
  File "find_islands.py", line 59, in <module>
    raise Exception("Too many arguments/files used as an input for find_islands.py")
Exception: Too many arguments/files used as an input for find_islands.py

* If you do not add input file then the following exception will be raised.
wlodarczyk@ubuntu:~/PycharmProjects/islands_on_water$ ./find_islands.sh 
Traceback (most recent call last):
  File "find_islands.py", line 62, in <module>
    raise Exception("Path to input file not given. Please add path to input file.")
Exception: Path to input file not given. Please add path to input file.


#### Unittests

* Go to test/ directory and run script below. In order to reduce the amount of code I wrote a test generator
wlodarczyk@ubuntu:~/PycharmProjects/islands_on_water$ cd test/
wlodarczyk@ubuntu:~/PycharmProjects/islands_on_water/test$ ./tests.sh 
............
----------------------------------------------------------------------
Ran 12 tests in 0.002s

OK

