import unittest
from sys import path
path.insert(0,'..')
from find_islands import Islands
from yaml import load

def test_generator(parameters):
    def test():
        map_grid, expected_result = parameters

        inst = Islands()
        map_grid = convert_test_string_to_map_grid(map_grid)
        result = inst.find_islands(map_grid)
        assert expected_result == result, "test info: : map_grid: {}," \
                                          "expected_result: {}," \
                                          " result: {}".format(map_grid,
                                                               expected_result, result)
    return test


def convert_test_string_to_map_grid(data):
    data = load(data)
    if data is None:
        map_grid = []
    elif isinstance(data, int):
        map_grid = [list(map(str, str(data)))]
    else:
        map_grid = [list(x) for x in data.split(" ")]
    return map_grid


suite = unittest.TestSuite()

test_cases = [
    ["""000000000
    010000000
    111000100
    110001110
    000001100
    001000000
    110000000
    000001100""", 4],
    ["""11110
    11010
    11000
    00000""", 1],
    ["""11000
    11000
    00100
    00011""", 1],
    ["""0""", 0],
    ["""""", 0],
    ["""1""", 1],
    ["""000000000""", 0],
    ["""111111111""", 1],
    ["""0000000001""", 1],
    ["""101010101""", 5],
    ["""2""", 0],
    ["""3333333333""", 0]

]

for parameters in test_cases:
    suite.addTest(
        unittest.FunctionTestCase(
            test_generator(parameters)))

unittest.TextTestRunner().run(suite)